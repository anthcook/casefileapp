'use strict';

angular.module('casefileApp', [
	'ngRoute',
	'casefile',
	'personfile'
]);
