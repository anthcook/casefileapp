'use strict';

angular.
	module('casefileApp').
	config(['$routeProvider',
		function config($routeProvider) {
			$routeProvider.
				when('/casefile', {
					template: '<casefile></casefile>'
				}).
				when('/personfile/:personId', {
					template: '<personfile></personfile>'
				}).
				otherwise('/casefile');
		}
	]);
