'use strict';

angular.
	module('core.arkcase.cases').
	factory('ArkCaseCases', ['$resource',
		function($resource) {
//			var hosturl = 'https://arkcase-ce.login/arkcase/j_spring_security_check';

//			return $resource('data/casefile-:caseId.json', {}, {
			return $resource(':hostUrl', {}, {
				query: {
					method: 'GET',
					params: {
						hostUrl: ''
//						caseId: ''
					},
					isArray: true
				}
			});
		}
	]);
