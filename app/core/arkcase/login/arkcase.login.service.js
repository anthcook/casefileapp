'use strict';

angular.
	module('core.arkcase.login').
	factory('ArkCaseLogin', ['$resource',
		function($resource) {
			var hosturl = 'https://arkcase-ce.login/arkcase/j_spring_security_check';
			var username = 'arkcase-admin@arkcase.org';
			var password = '@rKc@3e';
			var loginUrl = hosturl + '?j_username=' + username + '&j_password=' + password;
			return $resource(loginUrl, {}, {
				query: {
					method: 'POST',
					params: {},
					isArray: true
				}
			});
		}
	]);
