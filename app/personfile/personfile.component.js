'use strict';

angular.
	module('personfile').
	component('personfile', {
		templateUrl: 'personfile/personfile.template.html',
		controller: ['$routeParams', function PersonfileController($routeParams) {
			var self = this;
			
			self.version = '0.0.1';
			self.clientId = $routeParams.personId;
		}]
	});
