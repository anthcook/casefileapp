'use strict';

angular.
	module('casefile').
	component('casefile', {
		templateUrl: 'casefile/casefile.template.html',
		controller: ['ArkCaseCases', function CasefileController(ArkCaseCases) {
			var self = this;
			
			self.version = '0.1.4';
			self.hasResults = false;
			self.lastQueryId = '';
			self.casefile = {
				id: '',
				caseNumber: '',
				caseType: '',
				title: '',
				details: '',
				created: '',
				creator: '',
				client_id: '',
				client_name: '',
				client_email: '',
				personAssociations: []
			};

			// personAssociations: [{
			//   person: {
			//     id: '',
			//     givenName: '',
			//     middleName: '',
			//     familyName: '',
			//     contactMethods: [{
			//         type: '',
			//         subType: '',
			//         value: ''
			//     }],
			//     identifications: [{
			//         identificationType: '',
			//         identificationNumber: ''
			//     }]
			//   }
			// }]
			
			var onSuccess = function(response) {
				self.casefile = response;
				client:
				for (var i = 0; i < self.casefile.personAssociations.length; i++) {
					var person = self.casefile.personAssociations[i].person;
					for (var j = 0; j < person.contactMethods.length; j++) {
						if (person.contactMethods[j].type === 'email') {
							self.casefile.client_email = person.contactMethods[j].value;
							self.casefile.client_name = person.givenName +
									' ' + person.middleName +
									' ' + person.familyName;
							self.casefile.client_id = person.id;
							break client;
						}
					}
				}
				self.hasResults = true;
			};

			var onError = function(response) {
				if (response.status === 404) {
					alert( 'Case file ' + self.casefile.id + ' not found.' );
					self.casefile = {};
					self.hasResults = false;
				}
				else {
					alert( 'Unexpected status received: ' + response.status );
				}
			};

			self.getCasefile = function getCasefile() {
				if (self.casefile.id.toString() !== self.lastQueryId.toString()) {
					if (self.casefile.id.toString() !== '') {
						self.lastQueryId = self.casefile.id;
						var testUrl = 'data/casefile-' + self.casefile.id + '.json';
						var prodUrl = 'arkcase-ce.local/arkcase' +
								'/api/latest/plugin/casefile/byId/' +
								self.casefile.id;
						ArkCaseCases.get({hostUrl: testUrl}, onSuccess, onError);
//						ArkCaseCases.get({hostUrl: prodUrl}, onSuccess, onError);
					}
					else {
						self.casefile = {};
						self.hasResults = false;
					}
				}
				else {
					if (!self.hasResults) {
						self.casefile.id = '';
					}
				}
			};
		}]
	});
