'use strict';

describe('casefile', function() {
	
	beforeEach(module('casefile'));

	describe('CasefileController', function() {
		var httpBackend, ctrl;
		
		beforeEach(inject(function($componentController, $httpBackend) {
			httpBackend = $httpBackend;
			httpBackend.expectGET('data/casefile-101.json').
					respond({id: '101', caseNumber: '20210930_101', personAssociations: []});
			
			ctrl = $componentController('casefile');
		}));
		
		it('should create a `casefile` object fetched with `$http`', function() {
			expect(ctrl.casefile.id).toEqual('') &&
			expect(ctrl.casefile.caseNumber).toEqual('');
			
			ctrl.casefile.id = '101';
			ctrl.getCasefile();
			httpBackend.flush();
			
			expect(ctrl.casefile.id).toEqual('101') &&
			expect(ctrl.casefile.caseNumber).toEqual('20210930_101');
		});
	});
});
